const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err) return console.error('Unable to connect to MongoDb database.');
    console.log('Connected to MongoDB database.');
    const db = client.db('TodoApp');

    // db.collection('todos').find({_id: new ObjectID("5b4e1b795684ad38ba259998")}).toArray()
    //     .then(docs => {
    //         console.log('Todos');
    //         console.log(JSON.stringify(docs, undefined, 2));
    //     })
    //     .catch(e => {
    //         console.error('Unable to fetch todos', e);
    //     });

    db.collection('todos').find().count()
        .then(amount => {
            console.log(`Todos count: ${amount}`);
        })
        .catch(e => {
            console.error('Unable to fetch todos', e);
        });

    client.close();
});