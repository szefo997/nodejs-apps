const {ObjectID} = require('mongodb');
const {mongoose} = require('../server/db/mongoose');
const {Todo} = require('../server/models/todo');
const {User} = require('../server/models/user');

const id = '5b58a817facaeb78bb58c272';
const userId = '5b4e259a25402320cab4ca0d';

// if(!ObjectID.isValid(id)){
//     console.warn('not valid id');
// }

// Todo.find({
//         _id: id
//     })
//     .then(r => {
//         console.warn('find', r);
//     });
//
// Todo.findOne({
//         _id: id
//     })
//     .then(r => {
//         console.warn('findOne', r);
//     });

Todo.findById(id)
    .then(r => {
        console.warn('findById', r);
    })
    .catch(e => {
        console.error(e);
    });

User.findById(userId)
    .then(r => {
        console.warn('findById', r);
    })
    .catch(e => {
        console.error(e);
    });