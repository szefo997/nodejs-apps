const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err) return console.error('Unable to connect to MongoDb database.');
    console.log('Connected to MongoDB database.');
    const db = client.db('TodoApp');

    db.collection('todos')
        .findOneAndUpdate({'_id': new ObjectID("5b4e1b795684ad38ba259998")},
            {
                $set: {
                    completed: true
                }
            }, {
                returnOriginal: false
            })
        .then(r => {
            console.warn(r);
        });

    db.collection('todos')
        .findOneAndUpdate({'_id': new ObjectID("5b4e1b795684ad38ba259998")},
            {
                $set: {
                    name: 'Marian a'
                },
                $inc: {
                    age: 1
                }
            }, {
                returnOriginal: false
            })
        .then(r => {
            console.warn(r);
        });

    client.close();
});