const {MongoClient, ObjectID} = require('mongodb');

const obj = new ObjectID();
console.log('obj', obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err) return console.error('Unable to connect to MongoDb database.');
    console.log('Connected to MongoDB database.');
    const db = client.db('TodoApp');

    db.collection('todos').insertOne({
        text: 'Something to do 4',
        completed: false
    }, (err, result) => {
        if (err) return console.error('Unable to insert todo', err);
        console.log(JSON.stringify(result.ops, undefined, 2))
    });

    // db.collection('Users').insertOne({
    //     name: 'Marian',
    //     age: 29,
    //     location: 'Poznań'
    // }, (err, result) => {
    //     if (err) return console.error('Unable to insert user', err);
    //     // console.log(JSON.stringify(result.ops, undefined, 2))
    //     console.log(result.ops[0]._id.getTimeStamp());
    // });

    client.close();
});