const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err) return console.error('Unable to connect to MongoDb database.');
    console.log('Connected to MongoDB database.');
    const db = client.db('TodoApp');

    // db.collection('todos').deleteMany({'text': 'Something to do 3'})
    //     .then(r => {
    //         console.warn(r);
    //     });

    // db.collection('todos').deleteOne({'text': 'Something to do 4'})
    //     .then(r => {
    //         console.warn(r);
    //     });

    // db.collection('todos').findOneAndDelete({'text': 'Something to do 4'})
    //     .then(r => {
    //         console.warn(r);
    //     });

    client.close();
});