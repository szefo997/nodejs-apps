const {ObjectID} = require('mongodb');
const {mongoose} = require('../server/db/mongoose');
const {Todo} = require('../server/models/todo');
const {User} = require('../server/models/user');

const id = '5b58a817facaeb78bb58c272';
const userId = '5b4e259a25402320cab4ca0d';

Todo.remove()
    .then(r => {
        console.warn('remove', r);
    });

Todo.findOneAndRemove(id)
    .then(r => {
        console.warn('findOneAndRemove', r);
    })
    .catch(e => {
        console.error(e);
    });

User.findByIdAndRemove(userId)
    .then(r => {
        console.warn('findByIdAndRemove', r);
    })
    .catch(e => {
        console.error(e);
    });