const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');
const {User} = require('./../models/user');

const {app} = require('./../server');
const {Todo} = require('./../models/todo');
const {todos, populateTodos, users, populateUsers} = require('./seed/seed');

beforeEach(populateTodos);
beforeEach(populateUsers);

describe('POST /todos', () => {

    it('should create a new todo', done => {
        const text = 'test todo text';

        request(app)
            .post('/todos')
            .send({text})
            .expect('Content-Type', /json/)
            .expect(200)
            .expect((res) => {
                expect(res.body.text).toBe(text)
            })
            .end((err, res) => {
                if (err) return done(err);

                Todo.find({text: 'test todo text'})
                    .then(r => {
                        expect(r.length).toBe(1);
                        expect(r[0].text).toBe(text);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });
            });
    });

    it('should not create todo with invalid body data', done => {
        const text = '';

        request(app)
            .post('/todos')
            .send({text})
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);

                Todo.find()
                    .then(r => {
                        expect(r.length).toBe(2);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });
            });
    });

});

describe('GET /todos', () => {

    it('should get all todos', done => {

        request(app)
            .get('/todos')
            .expect(200)
            .expect(res => {
                expect(res.body.todos.length).toBe(2);
            })
            .end(done);
    });
});

describe('GET /todos/:id', () => {

    it('should return todo doc ', done => {

        request(app)
            .get(`/todos/${todos[0]._id.toHexString()}`)
            .expect(200)
            .expect(res => {
                expect(res.body.todo.text).toBe(todos[0].text);
            })
            .end(done);

    });

    it('it should return 404 (not valid id) ', done => {

        request(app)
            .get(`/todos/123`)
            .expect(404)
            .end(done);

    });

    it('it should return 404 (valid id) ', done => {

        request(app)
            .get(`/todos/${new ObjectID().toHexString()}`)
            .expect(404)
            .end(done);

    });

});

describe('DELETE /todos/:id', () => {

    it('should remove todo doc ', done => {

        const id = todos[0]._id.toHexString();
        request(app)
            .delete(`/todos/${id}`)
            .expect(200)
            .expect(res => {
                expect(res.body.todo._id).toBe(id);
            })
            .end((err, res) => {
                if (err) return done(err);

                Todo.findById(id)
                    .then(r => {
                        expect(r).toBe(null);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });

            });

    });

    it('should return 404 when id is not valid ', done => {

        request(app)
            .delete(`/todos/123`)
            .expect(404)
            .end(done);

    });

    it('should return 404 when document not found ', done => {

        request(app)
            .get(`/todos/${new ObjectID().toHexString()}`)
            .expect(404)
            .end(done);

    });

});

describe('PATCH /todos/:id', () => {

    it('should update todo doc ', done => {
        const id = todos[0]._id.toHexString();

        const text = 'test todo updated to true';
        request(app)
            .patch(`/todos/${id}`)
            .send({
                text,
                completed: true
            })
            .expect(200)
            .expect(res => {
                expect(res.body.todo._id).toBe(id);
                expect(res.body.todo.text).toBe(text);
            })
            .end((err, res) => {
                if (err) return done(err);

                Todo.findById(id)
                    .then(r => {
                        expect(r.text).toBe(text);
                        expect(r.completed).toBe(true);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });
            });

    });

    it('should clear completedAt when todo is not completed', done => {

        const id = todos[0]._id.toHexString();

        const text = 'test todo updated to false';
        request(app)
            .patch(`/todos/${id}`)
            .send({
                text,
                completed: false
            })
            .expect(200)
            .expect(res => {
                expect(res.body.todo._id).toBe(id);
                expect(res.body.todo.text).toBe(text);
                expect(res.body.todo.completed).toBe(false);
                expect(res.body.todo.completedAt).toBe(null);
            })
            .end((err, res) => {
                if (err) return done(err);

                Todo.findById(id)
                    .then(r => {
                        expect(r.text).toBe(text);
                        expect(r.completed).toBe(false);
                        expect(r.completedAt).toBe(null);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });

            });

    });

});

describe('GET /users/me', () => {

    it('should return user if authenticated', done => {

        console.warn('token', users[0].tokens[0].token);

        request(app)
            .get('/users/me')
            .set('x-auth', users[0].tokens[0].token)
            .expect(200)
            .expect(res => {
                expect(res.body._id).toBe(users[0]._id.toHexString());
                expect(res.body.email).toBe(users[0].email);
            })
            .end(done);

    });

    it('should return 401 if user is not authenticated', done => {

        request(app)
            .get('/users/me')
            .expect(401)
            .expect(res => {
                expect(res.body).toEqual({});
            }).end(done);

    });
});

describe('POST /user', () => {
    const email = 'marian_test@wp.pl';
    const password = 'testowe';

    it('should create a user', done => {

        request(app)
            .post('/users')
            .send({
                email,
                password
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .expect((res) => {
                expect(res.body.email).toBe(email)
            })
            .end((err, res) => {
                if (err) return done(err);

                User.findOne({email})
                    .then(r => {
                        expect(r.email).toBe(email);
                        done();
                    })
                    .catch(e => {
                        return done(e);
                    });
            });
    });

    it('should return validation errors if request is invalid', done => {

        request(app)
            .post('/users')
            .send({email: 'test', password})
            .expect(400)
            .end(done);

    });

    it('should not create user if email in use', done => {

        request(app)
            .post('/users')
            .send({email: users[0].email, password})
            .expect(400)
            .end(done);

    });

});
