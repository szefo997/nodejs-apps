const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;


const geocodeUrl = `https://maps.google.com/maps/api/geocode/json?address=${encodeURIComponent(argv.address)}`;

axios.get(geocodeUrl)
    .then(r => {
        if (r.data.status === 'ZERO_RESULTS') throw new Error('Unable to that address');
        if (r.data.status === 'OVER_QUERY_LIMIT') throw new Error('You have exceeded your daily request quota for this API.');

        const lat = r.data.results[0].geometry.location.lat;
        const lng = r.data.results[0].geometry.location.lng;
        return axios.get(`https://api.darksky.net/forecast/8f8303cc6deaf8dbd282c09fdb51671e/${lat},${lng}`);
    })
    .then(r => {
        const temperature = r.data.currently.temperature;
        const apparentTemperature = r.data.currently.apparentTemperature;
        console.log(`It's currently ${temperature}. It feels like ${apparentTemperature}`);
    })
    .catch(e => {
        if (e.code === 'ENOTFOUND')
            console.log('Unable to connect to API servers');
        else
            console.error(e.message);
    });


// https://api.darksky.net/forecast/8f8303cc6deaf8dbd282c09fdb51671e/37.8267,-122.4233
