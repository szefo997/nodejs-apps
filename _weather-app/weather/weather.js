const request = require('request');

const getWeather = (lat, lng, callback) => {
    request({
        url: `https://api.darksky.net/forecast/8f8303cc6deaf8dbd282c09fdb51671e/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if (!error && response.statusCode === 200)
            callback(undefined, {
                temperature : body.currently.temperature,
                apparentTemperature : body.currently.apparentTemperature
            });
        else
            callback('Unable to fetch weather.');
    });
};

module.exports = {
    getWeather
};


