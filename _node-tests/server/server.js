const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.status(404).send({
        error: 'page not found',
        name: 'todo app v1.0'
    });
});

app.get('/users', (req, res) => {
    res.status(200).send(
        [{
            name: 'Mike',
            age: 27
        },{
            name: 'Marian',
            age: 28
        },{
            name: 'Janusz',
            age: 29
        }]
    );
});

app.listen(3000);

module.exports.app = app;
