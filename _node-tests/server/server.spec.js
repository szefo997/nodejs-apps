const request = require('supertest');
const expect = require('expect');
const app = require('./server').app;

describe('Server', () => {

    it('should return 404', (done) => {
        request(app)
            .get('/')
            .expect(404)
            .expect(res => {
                expect(res.body).toInclude({
                    error: 'page not found'
                });
            })
            .end(done)
    });

    it('should return users array', (done) => {
        request(app)
            .get('/users')
            .expect(200)
            .expect(res => {
                expect(res.body)
                    .toBeA('array')
                    .toInclude({
                        name: 'Mike',
                        age: 27
                    });
            })
            .end(done)
    });

});




