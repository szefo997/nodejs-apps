const expect = require('expect');
const utils = require('./utils');

describe('Utils', () => {

    describe('#add', () => {

        it('should add two numbers', () => {
            const res = utils.add(33, 11);
            expect(res).toBe(44).toBeA('number');
        });

        it('should async add two numbers', (done) => {
            utils.asyncAdd(3, 4, (sum) => {
                expect(sum).toBe(7).toBeA('number');
                done();
            });
        });

    });

    it('should square a number', () => {
        const res = utils.square(4);
        expect(res).toBe(16).toBeA('number');
    });

    it('should contain first and last name', () => {
        const user = {location: 'Kraków', age: 55};
        const res = utils.setName(user, 'Marian Paździoch');

        expect(user.firstName).toBe('Marian').toBeA('string');
        expect(user.lastName).toBe('Paździoch').toBeA('string');
        expect(res).toEqual(
            {location: 'Kraków', age: 55, firstName: 'Marian', lastName: 'Paździoch'}
        ).toBeA('object');
    });

// it('should except some value', () => {
//     expect({name: 'Marian'}).toEqual({name: 'Marian'});
//     expect([1,2,3]).toInclude(2);
// });
});





