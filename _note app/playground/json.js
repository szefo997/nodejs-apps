const fs = require('fs');

const originalNote = {
    title: 'title',
    body: 'body'
};

const originalNoteString = JSON.stringify(originalNote);

fs.writeFileSync('notes.json', originalNoteString);

const noteString = fs.readFileSync('notes.json');
const note = JSON.parse(noteString);

// note
console.log(typeof note);
console.log(note.title);