const fs = require('fs');

function fetchNotes() {
    try {
        return JSON.parse(fs.readFileSync('notes-data.json'));
    } catch (e) {
        return [];
    }
}

function saveNotes(notes) {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
}

const addNote = (title, body) => {
    const notes = fetchNotes();

    for (const n of notes)
        if (n.title === title)
            return;

    const note = {
        title,
        body
    };
    notes.push(note);
    saveNotes(notes);
    return note;
};

const getAll = () => {
    console.log('getting all notes');
    return fetchNotes();
};

const getNote = (title) => {
    console.log(`getting note ${title}`);
    for (const n of fetchNotes())
        if (title === n.title)
            return n
};

const removeNote = (title) => {
    console.log(`remove note ${title}`);
    const notes = fetchNotes();
    let found = false;

    for (let i = 0; i < notes.length; i++) {
        if (title === notes[i].title) {
            notes.splice(i, 1);
            found = true;
            break;
        }
    }

    if (found) saveNotes(notes);
    return found;
};

const logNote = note => {
    debugger;
    console.log('--');
    console.log(`Title: ${note.title}`);
    console.log(`Body: ${note.body}`);
};

module.exports = {
    addNote,
    getAll,
    getNote,
    removeNote,
    logNote
};
